﻿$(document).ready(function () {
    $.get("http://demoorders.azurewebsites.net/orders", function (data) {
        var length = data.length;
        data.sort(function (a, b) {
            var aDate = new Date(a.date);
            var bDate = new Date(b.date);
            return bDate - aDate;
        });


        for (var i = 0; i < length; i++) {
            var date = new Date(data[i].date);
            if (data[i].status === "Creted") {
                data[i].status = "Created";
            }
            $('#result tr:last').after('<tr>'
                + '<td>' + data[i].id + '</td>'
                + '<td>' + data[i].customer + '</td>'
                + '<td>' + data[i].value + '</td>'
                + '<td>' + date.toJSON().slice(0, 10) + '</td>'
                + '<td>' + data[i].status + '</td>'
                 + '</tr>');
        }
    });

    $("#submit").on("click", function(e){
      e.preventDefault();
      console.log("hoho")

      $.ajax({
        type: "POST",
        url: "http://demoorders.azurewebsites.net/order",
        data: {
          customer: $("#customer").val(),
          value: $("#value").val(),
          date: Date.now(),
          status: "Created"
        },
        dataType: "json",
        success: function(response){
          if(response == true){
            $(".form-control").val("");
            $("#status p").html("Order successfully created");
            $("#status").addClass("alert-success").html("Order successfully created").show("slow");
          }
          else{
            $("#status").addClass("bg-danger").html("Something wrong").show("slow");
          }
        }
      });

    });
});
