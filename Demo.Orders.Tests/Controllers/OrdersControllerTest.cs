﻿using Demo.Orders.API.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Demo.Orders.API.Tests.Controllers
{
    [TestClass]
    public class OrdersControllerTest
    {
        [TestMethod]
        public void Get()
        {
            // Arrange
            OrdersController controller = new OrdersController();

            // Act
            IEnumerable<Models.Order> result = controller.GetAll();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Any(r => r.Status == Models.OrderStatus.Creted));
        }
    }
}