﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Demo.Orders.API.Models
{
    public class Order
    {
        public Guid Id { get; set; }

        public string Customer { get; set; }

        public decimal Value { get; set; }

        public DateTime Date { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public OrderStatus Status { get; set; }
    }
}