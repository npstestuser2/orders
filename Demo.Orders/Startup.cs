﻿using Microsoft.Owin;
using Newtonsoft.Json.Serialization;
using Owin;
using System.Net.Http.Headers;
using System.Web.Http;

[assembly: OwinStartup(typeof(Demo.Orders.API.Startup))]

namespace Demo.Orders.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            app.UseWebApi(config);
        }
    }
}