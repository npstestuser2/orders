﻿using Demo.Orders.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Demo.Orders.API.Controllers
{
    public class OrdersController : ApiController
    {
        private readonly Random random;

        public OrdersController()
        {
            this.random = new Random();
        }

        // GET /orders
        [Route("Orders")]
        [HttpGet]
        public IEnumerable<Models.Order> GetAll()
        {
            return GetAllOrders().OrderBy(o => o.Value);
        }

        // GET /orders/{id}
        [HttpGet]
        [Route("Orders/{id}")]
        public Models.Order GetById(Guid id)
        {
            return GetAllOrders().FirstOrDefault();
        }

        // POST /order
        [HttpPost]
        [Route("Order")]
        public bool Create(Models.Order order)
        {
            return true;
        }

        #region Helpers

        private IEnumerable<Models.Order> GetAllOrders()
        {
            int orderAmount = random.Next(10, 20);
            var orders = new List<Demo.Orders.API.Models.Order>();
            for (int i = 0; i < orderAmount; i++)
            {
                orders.Add(
                    new Models.Order
                    {
                        Id = Guid.NewGuid(),
                        Customer = String.Format("{0}, {1}", lastNames[random.Next(0, 4)], firstNames[random.Next(0, 4)]),
                        Date = DateTime.UtcNow.AddHours(-i * 2.4),
                        Status = i % 2 == 0 ? OrderStatus.Creted : OrderStatus.Delivered,
                        Value = random.Next(2000)
                    }
                );
            }
            return orders;
        }

        private string[] firstNames = { "John", "Abby", "Brian", "Al", "Angela" };

        private string[] lastNames = { "Johnson", "Smith", "Adams", "Williams", "Lewis" };

        #endregion Helpers
    }
}